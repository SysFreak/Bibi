<?php

require_once(__DIR__ . '/crest.php');

$result = CRest::installApp();

$botCode = 'bibiboomsolutions2022';

// handler for events "handler.php"
$handlerBackUrl = ($_SERVER['HTTPS'] === 'on' || $_SERVER['SERVER_PORT'] === '443' ? 'https' : 'http') . '://'
	. $_SERVER['SERVER_NAME']
	. (in_array($_SERVER['SERVER_PORT'],	['80', '443'], true) ? '' : ':' . $_SERVER['SERVER_PORT'])
	. str_replace($_SERVER['DOCUMENT_ROOT'], '', __DIR__)
	. '/handler.php';


// If is reinstall
// delete old bot
$botResult = CRest::call('imbot.bot.list');
if ($botResult['result']) {
	$botList = array_column($botResult['result'], 'ID', 'CODE');
	if ($botList[$botCode] > 0) {
		$t = CRest::call(
			'imbot.unregister',
			[
 				'BOT_ID' => $botList[$botCode],
			]
		);
	}
}


// If your application supports different localizations
// use $_REQUEST['data']['LANGUAGE_ID'] to load correct localization
// register new bot
$result = CRest::call(
	'imbot.register',
	[
		'CODE' => $botCode, // unique bot identifier  (req.)
		'TYPE' => 'B', // Bot type
		'OPENLINE' => 'Y',
		'EVENT_MESSAGE_ADD' => $handlerBackUrl, // Bot handler for new messages from user (req.)
		'EVENT_WELCOME_MESSAGE' => $handlerBackUrl, // Bot handler for joining to a chat (req.)
		'EVENT_BOT_DELETE' => $handlerBackUrl, // Bot handler for deleting bot (req.)
		'PROPERTIES' => [ // Bot personality (req.)
			'NAME' => 'Bibi', // Bot name (NAME or LAST_NAME is required)
			'LAST_NAME' => 'Bibi', // Bot last name
			'COLOR' => 'PINK', // Bot color for mobile Bitrix24 application RED,  GREEN, MINT, LIGHT_BLUE, DARK_BLUE, PURPLE, AQUA, PINK, LIME, BROWN,  AZURE, KHAKI, SAND, MARENGO, GRAY, GRAPHITE
			'EMAIL' => 'no@example.com',
			'PERSONAL_BIRTHDAY' => '1990-03-11', // format YYYY-mm-dd
			'WORK_POSITION' => 'Activo 24/7', // Bot 'job-title' as a bot description
			'PERSONAL_WWW' => 'https://www.bibibot.test',
			'PERSONAL_GENDER' => 'F', // Bot gender
			'PERSONAL_PHOTO' => base64_encode(file_get_contents(__DIR__ . '/images/profile.png')), // base64 bot avatar
		],
	]
);

if ($result['rest_only'] === false) : ?>

	<head>
		<script src="//api.bitrix24.com/api/v1/"></script>
		<?php if ($result['install'] == true) : ?>
			<script>
				BX24.init(function() {
					BX24.installFinish();
				});
			</script>
		<?php endif; ?>
	</head>

	<body>
		<?php if ($result['install'] == true) : ?>
			Instalación correcta
		<?php else : ?>
			installation error
		<?php endif; ?>
	</body>
<?php endif;
