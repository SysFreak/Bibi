<?php

class Mikrowisp
{
    public static function rif_exists($rif)
    {
        $token = 'xHFtzg2nYcBHcpinBexOEDmlQmrPJ3';
        $type = 99;

        $url = "https://boomsolutionsve.com/83DpQpcnFZQ3VAKLRMdm/bot/index.php?token=$token&type=$type&cedula=$rif";

        $curlSession = curl_init();

        curl_setopt($curlSession, CURLOPT_URL, $url);
        curl_setopt($curlSession, CURLOPT_RETURNTRANSFER, true);

        $response = curl_exec($curlSession);

        if (curl_errno($curlSession)) {
            $return = ["error" => "Algo salió mal: " . curl_error($curlSession)];
        } else {
            $return = json_decode($response, true);
        }

        return $return;
    }
}
