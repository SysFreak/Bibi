<?php

class Bitrix24
{
    public static function findAgentB24($depId)
    {

        $response = CRest::call('user.get', [
            'FILTER[UF_DEPARTMENT]' => $depId,
            'FILTER[IS_ONLINE]' => "Y"
        ]);

        if (empty($response['result'])) {
            $response = CRest::call('user.get', [
                'FILTER[UF_DEPARTMENT]' => $depId
            ]);
        }

        $result = $response['result'];
        $randIndex = array_rand($result, 1);
        $selectedAgent = $result[$randIndex]['ID'];

        return $selectedAgent;
    
    }

    public static function findAvailableAgentB24($depId)
    {
        $response = CRest::call('user.get', [
            'FILTER[UF_DEPARTMENT]' => $depId,
            'FILTER[IS_ONLINE]' => "Y"
        ]);

        if (empty($response['result'])) {
            $anyAvailabe = False;
            $selectedAgent = "";
        } else {
            $result = $response['result'];
            $randIndex = array_rand($result, 1);
            $selectedAgent = $result[$randIndex]['ID'];
            $anyAvailabe = true;
        }

        return [$anyAvailabe, $selectedAgent];
    
    }

    public static function createTask($fields = [])
    {
        //Rest creates task
        CRest::call('tasks.task.add', [
            'fields' => [
                'TITLE' => $fields['TITLE'],
                'DESCRIPTION_IN_BBCODE' => 'Y',
                'DESCRIPTION' => $fields['DESCRIPTION'],
                'GROUP_ID' => $fields['GROUP_ID'],
                'CREATED_BY' => $fields['CREATED_BY'],
                'RESPONSIBLE_ID' => $fields['RESPONSIBLE_ID'],
                'UF_CRM_TASK' => [$fields['UF_CRM_TASK']]
            ]
        ]);
    
    }

    public static function createLead($arLead = [])
    {
        $title  =   $arLead['NAMES'] . " - " . $arLead['TEL'];
        $name   =   $arLead['NAMES'];
        $tel    =   $arLead['TEL'];
        $responsibleId = $arLead['RESPONSIBLE'];

        $result = CRest::call(
            'crm.lead.add',
            [
                'fields' => [
                    'TITLE' => $title,
                    'NAME' => $name,
                    'PHONE' => [
                        [
                            'VALUE' => $tel,
                            'VALUE_TYPE' => 'MOBILE'
                        ]
                    ],
                    'ASSIGNED_BY_ID' => $responsibleId
                ]
            ]
        );

        $leadIdCreated = $result['result'];


        return $leadIdCreated;
    
    }

    public static function createLeadFirst($arLead = [])
    {
        $title  =   $arLead['NAMES'] . " - " . $arLead['TEL'];
        $name   =   $arLead['NAMES'];
        // $tel    =   preg_replace('/[0-9\@\.\;\" "]+/', '', $arLead['TEL']);
        $tel    =   $arLead['TEL'];
        $dni    =   $arLead['DNI'];
        $responsibleId = $arLead['RESPONSIBLE'];

        
        $query  =   'INSERT INTO b24_clients (client, phone, dni, client_id, created_at) VALUES("'.strtoupper($name).'", "'.$tel.'", "'.$dni.'", 0, "'.date("Y-m-d H:m:s").'")';
        $lID    =   DBBot::DataExecuteLastID($query);

        $query  =   'SELECT * FROM b24_clients WHERE dni LIKE "%'.$dni.'%"';
        $iData  =   DBBot::DBQueryAll($query);

        if($iData)
        {
            $query  =   'SELECT client_id FROM b24_clients WHERE dni LIKE "%'.$dni.'%" AND phone LIKE "%'.$tel.'%" AND client_id <> 0 ORDER BY id DESC LIMIT 1';
            $info   =   DBBot::DBQuery($query);

            if($info)
            {
                $query  =   'UPDATE b24_clients SET client_id = "'.$info['client_id'].'", status_id = "1" WHERE dni LIKE "%'.$dni.'%" AND phone LIKE "%'.$tel.'%"';
                $upd    =   DBBot::DataExecute($query);
                
                $leadIdCreated  =   $info['client_id'];
            }else{
                // $id     =   rand(10000, 99999);

                $result = CRest::call(
                    'crm.lead.add',
                    [
                        'fields' => [
                            'TITLE' => 'ChatBot - ' . $title,
                            'NAME' => $name,
                            'PHONE' => [
                                [
                                    'VALUE' => '+58'.$tel,
                                    'VALUE_TYPE' => 'MOBILE'
                                ]
                            ],
                            'UF_CRM_1609794790022'  =>  69,
                            'ASSIGNED_BY_ID' => $responsibleId
                        ]
                    ]
                );

                $query  =   'UPDATE b24_clients SET client_id = "'.$result['result'].'", status_id = "1" WHERE dni LIKE "%'.$dni.'%" AND phone LIKE "%'.$tel.'%"';
                $upd    =   DBBot::DataExecute($query);

                $leadIdCreated  =   $result['result'];
            }
        }

        return $leadIdCreated;
    
    }

    public static function leadExists($phone = '')
    {

        $result = CRest::call('crm.lead.list', [
            'FILTER[PHONE]' => $phone
        ]);
    
    }

    public static function transferOperator($chatId, $operator)
    {

        CRest::call('imbot.chat.user.add', [
            'CHAT_ID' => $chatId,
            'TRANSFER_ID' => $operator
        ]);

    }

    public static function CheckPhone($data)
    {
        $len    =   strlen($data);

        if( $len <= 14 && $len >= 13 ) {   return substr($data, -7); } elseif ( $len <= 12 && $len >= 10 ) { return substr($data, -7); }elseif ( $len <= 10 && $len >= 7 ) { return substr($data, -7); }else{ return false; }
    
    }
}
