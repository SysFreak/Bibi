<?php

const BOT_PROFILE_ID = 1400;

class Bot
{

    public static function testClass($msg)
    {
        $tel = "987654323";
        $client = "Pepito Perez Pascal";

        $actionType = "";
        $badOptionTxt = "";

        $selectedAgent = Bitrix24::findAgentB24(SERVICE_VENEZUELA_DEPARTMENT_ID);

        $clientBD = Context::getUserInfo("1418");
        $whereMove = $clientBD['WHERE_MOVE'];
        $addressMove = $clientBD['ADDRESS_MOVE'];
        $namesAPI = API_MIKROWISP['NAMES'];
        $idAPI = API_MIKROWISP['ID'];


        $fields = [
            'TITLE' => "Cliente: " . $namesAPI . " - Mikrowisp: " . $idAPI . " - Solicita Mudanza de Equipos",
            'DESCRIPTION' => "Cliente se comunica solicitando Mudanza de Equipos, bajo los siguientes parámetros" . "\n" . "\n" .
                "Título: " . "[B]Mudanza de Equipos[/B]" . "\n" .
                "Cliente: " . "[B]" . $namesAPI . "[/B]"  . "\n" .
                "Mikrowisp: " . "[B]" . $idAPI . "[/B]"  . "\n" .
                "Tipo: " . "[B]" . $whereMove . "[/B]"  . "\n" .
                "Dirección: " . "[B]" . $addressMove . "[/B]",
            'GROUP_ID' => SERVICE_VENEZUELA_WORKGROUP_ID,
            'CREATED_BY' => TASK_CREATOR_ID,
            'RESPONSIBLE_ID' => $selectedAgent,
            'UF_CRM_TASK' => ""
        ];


        $result = Bitrix24::createTask($fields);

        //That action has not any message, so we advance next action

        $info = [$selectedAgent,  $whereMove, $addressMove, $namesAPI,  $idAPI];

        $fields = json_encode($fields);
        $info = json_encode($info);
        $result = json_encode($result);
        //  $selectedAgent = json_encode($selectedAgent);
        return ["Test Class Bot que llama a CREST y ejecuta crm.lead.add y como resultado lo siguiente: ", $result, $info, $fields];
    }


    public static function executeAction($chatId, $actualContextActions, $actualAction, $clientMsg)
    {

        $messages = [];
        $changeAction = false; //To know if keep bucle or stop
        $justMssg = true;  //To Know if executeAction returns just message or executes some action
        $resultAction = [];
        if (method_exists('Bot', $actualAction)) {
            $resultAction = static::$actualAction($chatId, $actualContextActions, $actualAction, $clientMsg);
            $justMssg = false;
        } else {
            foreach ($actualContextActions as $key => $value) {

                if ($key == $actualAction) {
                    $changeAction = true;
                }
                if ($changeAction) {
                    if (method_exists('Bot', $key)) {
                        $changeAction = false; //to control that the next wont pass
                        $messages[] = $value;
                        $actualAction = $key;
                    } else {
                        $messages[] = $value;
                        $actualAction = $key;
                    }
                }
            }
            //Changing the current action for the Chat ID User
            static::updateUserBD($chatId, "ACTION", $actualAction);
            $justMssg = true;
        }

        return [$justMssg, $messages, $resultAction];
    }

    protected static function findAgentAndCreateTask_toCall($chatId, $actualContextActions = '', $actualAction = '', $clientMsg)
    {
        $actionType = "";
        $badOptionTxt = "";

        $selectedAgent = Bitrix24::findAgentB24(SALES_VENEZUELA_DEPARTMENT_ID);
        $clientBD = Context::getUserInfo($chatId);
        $telephone = $clientBD['PHONE'];
        $names = $clientBD['NAMES'];

        $fields = [
            'TITLE'             =>  "Llamar a cliente - Información de servicio - " . $telephone,
            'DESCRIPTION'       =>  "Cliente solicita llamada telefónica solicitando información del Servicio." . "\n" . "\n" .
                "[B]Cliente:[/B] " . $names . "\n" .
                "[B]Teléfono:[/B] " . $telephone,
            'GROUP_ID'          =>  SALES_VENEZUELA_WORKGROUP_ID,
            'CREATED_BY'        =>  TASK_CREATOR_ID,
            'RESPONSIBLE_ID'    =>  $selectedAgent,
            'UF_CRM_TASK'       =>  []
        ];

        Bitrix24::createTask($fields);

        //That action has not any message, so we advance next action
        static::nextAction($chatId, $actualAction, $actualContextActions);
        $actionType = "voidMsg";

        return [$actionType, $badOptionTxt];
    }

    protected static function updateUserBD($chatId, $whatChange, $newInfo)
    {
        $path = dirname(__FILE__, 3) . "/" . "json/";
        $file = $chatId . ".json"; //information of ChatID in a Json file
        $arFile = Context::getFile($path, $file);

        switch ($whatChange) {
            case "CONTEXT":
                $arFile["CONTEXT"]              = $newInfo;
                break;
            case "ACTION":
                $arFile["ACTION"]               = $newInfo;
                break;
            case "NAMES":
                $arFile["NAMES"]                = $newInfo;
                break;
            case "PHONE":
                $arFile["PHONE"]                = $newInfo;
                break;
            case "DNI":
                $arFile["DNI"]                  = $newInfo;
                break;
            case "LAST_CONVERSATION":
                $arFile["LAST_CONVERSATION"]    = $newInfo;
                break;
            case "RIF":
                $arFile["RIF"]                  = $newInfo;
                break;
            case "ATTEMPTS":
                $arFile["ATTEMPTS"]             = $newInfo;
                break;
            case "PHONE_REF":
                $arFile["PHONE_REF"]            = $newInfo;
                break;
            case "NAMES_REF":
                $arFile["NAMES_REF"]            = $newInfo;
                break;
            case "RIF_TRIES":
                $arFile["RIF_TRIES"]            = $newInfo;
                break;
            case "WHATSAPP_TRIES":
                $arFile["WHATSAPP_TRIES"]       = $newInfo;
                break;
            case "WHERE_MOVE":
                $arFile["WHERE_MOVE"]           = $newInfo;
                break;
            case "ADDRESS_MOVE":
                $arFile["ADDRESS_MOVE"]         = $newInfo;
                break;
            case "ID_API":
                $arFile["ID_API"]               = $newInfo;
                break;
            case "NAMES_API":
                $arFile["NAMES_API"]            = $newInfo;
                break;
            case "STATE_API":
                $arFile["STATE_API"]            = $newInfo;
                break;
            case "PHONE_API":
                $arFile["PHONE_API"]            = $newInfo;
                break;
            case "PASSWORD_API":
                $arFile["PASSWORD_API"]         = $newInfo;
                break;
            case "SERVICE_API":
                $arFile["SERVICE_API"]          = $newInfo;
                break;
            case "FACNP_API":
                $arFile["FACNP_API"]            = $newInfo;
                break;
            case "FACTOTAL_API":
                $arFile["FACTOTAL_API"]         = $newInfo;
                break;
            case "NEW_PASS":
                $arFile["NEW_PASS"]             = $newInfo;
                break;
            case "NEW_NAMENET":
                $arFile["NEW_NAMENET"]          = $newInfo;
                break;
            case "FAIL":
                $arFile["FAIL"]                 = $newInfo;
                break;
            case "FAIL_DESC":
                $arFile["FAIL_DESC"]            = $newInfo;
                break;
            case "CLAIM":
                $arFile["CLAIM"]                = $newInfo;
                break;
            case "CLAIM_DESC":
                $arFile["CLAIM_DESC"]           = $newInfo;
                break;
        }
        Context::setFile($path, $file, $arFile);
    }

    protected static function nextAction($chatId, $actualAction, $actualContextActions)
    {
        $takeNext = false;
        foreach ($actualContextActions as $key => $value) {
            if ($takeNext) {
                static::updateUserBD($chatId, "ACTION", $key);
                $takeNext = false;
            }
            if ($key == $actualAction) {
                $takeNext = true;
            }
        }
    }


    public static function prevMenu($chatId)
    {
        $context = Context::contextNow($chatId);
        $pos = strrpos($context, "|");
        $oldContext = substr($context, 0, $pos);
        static::updateUserBD($chatId, "CONTEXT", $oldContext);
        $newContextActions = Context::getContextActions($oldContext);
        static::updateUserBD($chatId, "ACTION", array_key_last($newContextActions));
    }

    protected static function nextMenu($chatId, $newMenu)
    {
        $context = Context::contextNow($chatId);
        static::updateUserBD($chatId, "CONTEXT", $context . $newMenu);
        $newContextActions = Context::getContextActions($context . $newMenu);
        static::updateUserBD($chatId, "ACTION", array_key_first($newContextActions));
    }

    protected static function sendMenu1121121($chatId, $actualContextActions = '', $actualAction = '', $clientMsg)
    {
        $actionType = "";
        $badOptionTxt = "";

        switch ($clientMsg) {
            case '1':
                static::nextMenu($chatId, "|MENU_1.1.2.1.1.2.1.1");
                $actionType = "requestFinal";
                break;
            case '2':
                static::nextMenu($chatId, "|MENU_1.1.2.1.1.2.1.2");
                $actionType = "requestFinal";
                break;
            default:
                $actionType = "badOption";
                break;
        }
        return [$actionType, $badOptionTxt];
    }

    protected static function sendMenu112112($chatId, $actualContextActions = '', $actualAction = '', $clientMsg)
    {
        $actionType = "";
        $badOptionTxt = "";

        switch ($clientMsg) {
            case '1':
                static::nextMenu($chatId, "|MENU_1.1.2.1.1.2.1");
                $actionType = "sendMenu";
                break;
            case '2':
                static::prevMenu($chatId);
                $actionType = "prevMenu";
                break;
            default:
                $actionType = "badOption";
                break;
        }
        return [$actionType, $badOptionTxt];
    }
    
    protected static function sendMenu1121111($chatId, $actualContextActions = '', $actualAction = '', $clientMsg)
    {
        $actionType = "";
        $badOptionTxt = "";

        switch ($clientMsg) {
            case '1':
                static::nextMenu($chatId, "|MENU_1.1.2.1.1.1.1.1");
                $actionType = "requestFinal";
                break;
            case '2':
                static::nextMenu($chatId, "|MENU_1.1.2.1.1.1.1.2");
                $actionType = "requestFinal";
                break;
            default:
                $actionType = "badOption";
                break;
        }
        return [$actionType, $badOptionTxt];
    }

    protected static function sendMenu112111($chatId, $actualContextActions = '', $actualAction = '', $clientMsg)
    {
        $actionType = "";
        $badOptionTxt = "";

        switch ($clientMsg) {
            case '1':
                static::nextMenu($chatId, "|MENU_1.1.2.1.1.1.1");
                $actionType = "sendMenu";
                break;
            case '2':
                static::prevMenu($chatId);
                $actionType = "prevMenu";
                break;
            default:
                $actionType = "badOption";
                break;
        }
        return [$actionType, $badOptionTxt];
    }

    protected static function sendMenu11222($chatId, $actualContextActions = '', $actualAction = '', $clientMsg)
    {
        $actionType = "";
        $badOptionTxt = "";

        switch ($clientMsg) {
            case '1':
                static::nextMenu($chatId, "|MENU_1.1.2.2.2.1");
                $actionType = "requestFinal";
                break;
            case '2':
                static::nextMenu($chatId, "|MENU_1.1.2.2.2.2");
                $actionType = "requestFinal";
                break;
            default:
                $actionType = "badOption";
                break;
        }
        return [$actionType, $badOptionTxt];
    }

    protected static function sendMenu1122($chatId, $actualContextActions = '', $actualAction = '', $clientMsg)
    {
        $actionType = "";
        $badOptionTxt = "";

        switch ($clientMsg) {
            case '1':
                static::nextMenu($chatId, "|MENU_1.1.2.2.1");
                $actionType = "firstJustActionThenBye";
                break;
            case '2':
                static::nextMenu($chatId, "|MENU_1.1.2.2.2");
                $actionType = "sendMenu";
                break;
            case '3':
                static::prevMenu($chatId);
                $actionType = "prevMenu";
                break;
            default:
                $actionType = "badOption";
                break;
        }
        return [$actionType, $badOptionTxt];
    }

    protected static function sendMenu11211($chatId, $actualContextActions = '', $actualAction = '', $clientMsg)
    {
        $actionType = "";
        $badOptionTxt = "";

        switch ($clientMsg) {
            case '1':
                static::nextMenu($chatId, "|MENU_1.1.2.1.1.1");
                $actionType = "sendMenu";
                break;
            case '2':
                static::nextMenu($chatId, "|MENU_1.1.2.1.1.2");
                $actionType = "sendMenu";
                break;
            case '3':
                static::prevMenu($chatId);
                $actionType = "prevMenu";
                break;
            default:
                $actionType = "badOption";
                break;
        }
        return [$actionType, $badOptionTxt];
    }

    protected static function sendMenu11212($chatId, $actualContextActions = '', $actualAction = '', $clientMsg)
    {
        $actionType = "";
        $badOptionTxt = "";

        switch ($clientMsg) {
            case '1':
                static::nextMenu($chatId, "|MENU_1.1.2.1.2.1");
                $actionType = "sendMenu";
                break;
            case '2':
                static::nextMenu($chatId, "|MENU_1.1.2.1.2.2");
                $actionType = "sendMenu";
                break;
            case '3':
                static::prevMenu($chatId);
                $actionType = "prevMenu";
                break;
            default:
                $actionType = "badOption";
                break;
        }
        return [$actionType, $badOptionTxt];
    }

    protected static function sendMenu1121221($chatId, $actualContextActions = '', $actualAction = '', $clientMsg)
    {
        $actionType = "";
        $badOptionTxt = "";

        switch ($clientMsg) {
            case '1':
                static::nextMenu($chatId, "|MENU_1.1.2.1.2.2.1.1");
                $actionType = "requestFinal";
                break;
            case '2':
                static::nextMenu($chatId, "|MENU_1.1.2.1.2.2.1.2");
                $actionType = "requestFinal";
                break;
            default:
                $actionType = "badOption";
                break;
        }
        return [$actionType, $badOptionTxt];
    }

    protected static function sendMenu112122($chatId, $actualContextActions = '', $actualAction = '', $clientMsg)
    {
        $actionType = "";
        $badOptionTxt = "";

        switch ($clientMsg) {
            case '1':
                static::nextMenu($chatId, "|MENU_1.1.2.1.2.2.1");
                $actionType = "sendMenu";
                break;
            case '2':
                static::prevMenu($chatId);
                $actionType = "prevMenu";
                break;
            default:
                $actionType = "badOption";
                break;
        }
        return [$actionType, $badOptionTxt];
    }

    protected static function sendMenu112121($chatId, $actualContextActions = '', $actualAction = '', $clientMsg)
    {
        $actionType = "";
        $badOptionTxt = "";

        switch ($clientMsg) {
            case '1':
                static::nextMenu($chatId, "|MENU_1.1.2.1.2.1.1");
                $actionType = "sendMenu";
                break;
            case '2':
                static::prevMenu($chatId);
                $actionType = "prevMenu";
                break;
            default:
                $actionType = "badOption";
                break;
        }
        return [$actionType, $badOptionTxt];
    }

    protected static function sendMenu1121211($chatId, $actualContextActions = '', $actualAction = '', $clientMsg)
    {
        $actionType = "";
        $badOptionTxt = "";

        switch ($clientMsg) {
            case '1':
                static::nextMenu($chatId, "|MENU_1.1.2.1.2.1.1.1");
                $actionType = "requestFinal";
                break;
            case '2':
                static::nextMenu($chatId, "|MENU_1.1.2.1.2.1.1.2");
                $actionType = "requestFinal";
                break;
            default:
                $actionType = "badOption";
                break;
        }
        return [$actionType, $badOptionTxt];
    }

    protected static function sendMenu112131($chatId, $actualContextActions = '', $actualAction = '', $clientMsg)
    {
        $actionType = "";
        $badOptionTxt = "";

        switch ($clientMsg) {
            case '1':
                static::nextMenu($chatId, "|MENU_1.1.2.1.3.1.1");
                $actionType = "requestFinal";
                break;
            case '2':
                static::nextMenu($chatId, "|MENU_1.1.2.1.3.1.2");
                $actionType = "requestFinal";
                break;
            default:
                $actionType = "badOption";
                break;
        }
        return [$actionType, $badOptionTxt];
    }

    protected static function sendMenu11213($chatId, $actualContextActions = '', $actualAction = '', $clientMsg)
    {
        $actionType = "";
        $badOptionTxt = "";

        switch ($clientMsg) {
            case '1':
                static::nextMenu($chatId, "|MENU_1.1.2.1.3.1");
                $actionType = "sendMenu";
                break;
            case '2':
                static::prevMenu($chatId);
                $actionType = "prevMenu";
                break;
            default:
                $actionType = "badOption";
                break;
        }
        return [$actionType, $badOptionTxt];
    }

    protected static function sendMenu112141($chatId, $actualContextActions = '', $actualAction = '', $clientMsg)
    {
        $actionType = "";
        $badOptionTxt = "";

        switch ($clientMsg) {
            case '1':
                static::nextMenu($chatId, "|MENU_1.1.2.1.4.1.1");
                $actionType = "requestFinal";
                break;
            case '2':
                static::nextMenu($chatId, "|MENU_1.1.2.1.4.1.2");
                $actionType = "requestFinal";
                break;
            default:
                $actionType = "badOption";
                break;
        }
        return [$actionType, $badOptionTxt];
    }

    protected static function sendMenu11214($chatId, $actualContextActions = '', $actualAction = '', $clientMsg)
    {
        $actionType = "";
        $badOptionTxt = "";

        switch ($clientMsg) {
            case '1':
                static::nextMenu($chatId, "|MENU_1.1.2.1.4.1");
                $actionType = "sendMenu";
                break;
            case '2':
                static::prevMenu($chatId);
                $actionType = "prevMenu";
                break;
            default:
                $actionType = "badOption";
                break;
        }
        return [$actionType, $badOptionTxt];
    }

    protected static function sendMenu112151($chatId, $actualContextActions = '', $actualAction = '', $clientMsg)
    {
        $actionType = "";
        $badOptionTxt = "";

        switch ($clientMsg) {
            case '1':
                static::nextMenu($chatId, "|MENU_1.1.2.1.5.1.1");
                $actionType = "requestFinal";
                break;
            case '2':
                static::nextMenu($chatId, "|MENU_1.1.2.1.5.1.2");
                $actionType = "requestFinal";
                break;
            default:
                $actionType = "badOption";
                break;
        }
        return [$actionType, $badOptionTxt];
    }

    protected static function sendMenu11215($chatId, $actualContextActions = '', $actualAction = '', $clientMsg)
    {
        $actionType = "";
        $badOptionTxt = "";

        switch ($clientMsg) {
            case '1':
                static::nextMenu($chatId, "|MENU_1.1.2.1.5.1");
                $actionType = "sendMenu";
                break;
            case '2':
                static::prevMenu($chatId);
                $actionType = "prevMenu";
                break;
            default:
                $actionType = "badOption";
                break;
        }
        return [$actionType, $badOptionTxt];
    }

    protected static function sendMenu11216($chatId, $actualContextActions = '', $actualAction = '', $clientMsg)
    {
        $actionType = "";
        $badOptionTxt = "";

        switch ($clientMsg) {
            case '1':
                static::nextMenu($chatId, "|MENU_1.1.2.1.6.1");
                $actionType = "requestFinal";
                break;
            case '2':
                static::nextMenu($chatId, "|MENU_1.1.2.1.6.2");
                $actionType = "requestFinal";
                break;
            default:
                $actionType = "badOption";
                break;
        }
        return [$actionType, $badOptionTxt];
    }

    protected static function sendMenu1121($chatId, $actualContextActions = '', $actualAction = '', $clientMsg)
    {
        $actionType = "";
        $badOptionTxt = "";
        switch ($clientMsg) {
            case '1':
                static::nextMenu($chatId, "|MENU_1.1.2.1.1");
                $actionType = "sendMenu";
                break;
            case '2':
                static::nextMenu($chatId, "|MENU_1.1.2.1.2");
                $actionType = "sendMenu";
                break;
            case '3':
                static::nextMenu($chatId, "|MENU_1.1.2.1.3");
                $actionType = "sendMenu";
                break;
            case '4':
                static::nextMenu($chatId, "|MENU_1.1.2.1.4");
                $actionType = "sendMenu";
                break;
            case '5':
                static::nextMenu($chatId, "|MENU_1.1.2.1.5");
                $actionType = "sendMenu";
                break;
            case '6':
                static::nextMenu($chatId, "|MENU_1.1.2.1.6");
                $actionType = "sendMenu";
                break;
            case '7':
                static::prevMenu($chatId);
                $actionType = "prevMenu";
                break;
            default:
                $actionType = "badOption";
                break;
        }
        return [$actionType, $badOptionTxt];
    }

    protected static function findAvailableAgentToChat($chatId, $actualContextActions = '', $actualAction = '', $clientMsg)
    {
        $actionType = "";
        $badOptionTxt = "";

        $selectedAgent = Bitrix24::findAvailableAgentB24(SOCIAL_VENEZUELA_DEPARTMENT_ID);
        $selectedAgentExists = $selectedAgent[0];

        if (!$selectedAgentExists) {
            $whatsAppTries = Context::getUserInfo($chatId)['WHATSAPP_TRIES'];
            if ($whatsAppTries == 0) {
                static::updateUserBD($chatId, "WHATSAPP_TRIES", 1);
                $actionType = "notFoundAvailable";
            } elseif ($whatsAppTries == 1) {
                static::updateUserBD($chatId, "WHATSAPP_TRIES", 2);
                $actionType = "notFoundAvailable";
            } elseif ($whatsAppTries == 2) {
                static::updateUserBD($chatId, "WHATSAPP_TRIES", 0);
                $actionType = "notFoundAvailable2";
            }
        } else {
            static::updateUserBD($chatId, "ACTION", "byeBot");
            $actionType = "transferToOperator";
            $selectedAgentId = $selectedAgent[1];
        }

        return [$actionType, $selectedAgentId];
    }

    protected static function sendMenu1124($chatId, $actualContextActions = '', $actualAction = '', $clientMsg)
    {
        $actionType = "";
        $badOptionTxt = "";

        switch ($clientMsg) {
            case '1':
                static::nextMenu($chatId, "|MENU_1.1.2.4.1");
                $actionType = "firstJustActionThenBye";
                break;
            case '2':
                static::nextMenu($chatId, "|MENU_1.1.2.4.2");
                $actionType = "validateWhatsApp";
                break;
            case '3':
                static::prevMenu($chatId);
                $actionType = "prevMenu";
                break;
            default:
                $actionType = "badOption";
                break;
        }
        return [$actionType, $badOptionTxt];
    }

    protected static function sendMenu112($chatId, $actualContextActions = '', $actualAction = '', $clientMsg)
    {
        $actionType = "";
        $badOptionTxt = "";
        switch ($clientMsg) {
            case '1': //Pass the context MENU_1.1
                static::nextMenu($chatId, "|MENU_1.1.2.1");
                $actionType = "sendMenu";
                break;
            case '2':
                static::nextMenu($chatId, "|MENU_1.1.2.2");
                $actionType = "sendMenu";
                break;
            case '3':
                static::nextMenu($chatId, "|MENU_1.1.2.3");
                $actionType = "requestFinal";
                break;
            case '4':
                static::nextMenu($chatId, "|MENU_1.1.2.4");
                $actionType = "sendMenu";
                break;
            case '5':
                static::prevMenu($chatId);
                $actionType = "prevMenu";
                break;
            default:
                $actionType = "badOption";
                $badOptionTxt = "A ingresado un valor incorrecto[BR][BR]";
                break;
        }
        return [$actionType, $badOptionTxt];
    }

    protected static function sendMenu1112($chatId, $actualContextActions = '', $actualAction = '', $clientMsg)
    {
        $actionType = "";
        $badOptionTxt = "";
        switch ($clientMsg) {
            case '1': //Pass the context MENU_1.1
                static::nextMenu($chatId, "|MENU_1.1.1.2.1");
                $actionType = "sendMenu";
                break;
            case '2':
                static::nextMenu($chatId, "|MENU_1.1.1.2.2");
                $actionType = "sendMenu";
                break;

            case '3':
                static::prevMenu($chatId);
                $actionType = "prevMenu";
                break;
            default:
                $actionType = "badOption";
                $badOptionTxt = "A ingresado un valor incorrecto[BR][BR]";
                break;
        }
        return [$actionType, $badOptionTxt];
    }

    public static function findAgentAndCreatePassChangeTask($chatId, $actualContextActions = '', $actualAction = '', $clientMsg)
    {
        $actionType = "";
        $badOptionTxt = "";

        $selectedAgent = Bitrix24::findAgentB24(SERVICE_VENEZUELA_DEPARTMENT_ID);

        $clientBD = Context::getUserInfo($chatId);
        $newPass = $clientBD['NEW_PASS'];
        $namesAPI = $clientBD['NAMES_API'];
        $idAPI = $clientBD['ID_API'];


        $fields = [
            'TITLE' => "Cliente: " . $namesAPI . " - Mikrowisp: " . $idAPI . " - Solicita cambio de Password de Red",
            'DESCRIPTION' => "Cliente se comunica solicitando Cambio de Password de Red, bajo los siguientes parámetros" . "\n" . "\n" .
                "Título: " . "[B]Cambio de Password de Red[/B]" . "\n" .
                "Cliente: " . "[B]" . $namesAPI . "[/B]"  . "\n" .
                "Mikrowisp: " . "[B]" . $idAPI . "[/B]"  . "\n" .
                "Password: " . "[B]" . $newPass . "[/B]",
            'GROUP_ID' => SERVICE_VENEZUELA_WORKGROUP_ID,
            'CREATED_BY' => TASK_CREATOR_ID,
            'RESPONSIBLE_ID' => $selectedAgent,
            'UF_CRM_TASK' => ""
        ];


        Bitrix24::createTask($fields);

        //That action has not any message, so we advance next action
        static::nextAction($chatId, $actualAction, $actualContextActions);
        $actionType = "voidMsg";

        return [$actionType, $badOptionTxt];
    }

    public static function findAgentAndCreateNamenetChangeTask($chatId, $actualContextActions = '', $actualAction = '', $clientMsg)
    {
        $actionType = "";
        $badOptionTxt = "";

        $selectedAgent = Bitrix24::findAgentB24(SERVICE_VENEZUELA_DEPARTMENT_ID);

        $clientBD = Context::getUserInfo($chatId);
        $newNamenet = $clientBD['NEW_NAMENET'];
        $namesAPI = $clientBD['NAMES_API'];
        $idAPI = $clientBD['ID_API'];


        $fields = [
            'TITLE' => "Cliente: " . $namesAPI . " - Mikrowisp: " . $idAPI . " - Solicita cambio de Nombre de Red",
            'DESCRIPTION' => "Cliente se comunica solicitando Cambio de Nombre de Red, bajo los siguientes parámetros" . "\n" . "\n" .
                "Título: " . "[B]Cambio de Nombre de Red[/B]" . "\n" .
                "Cliente: " . "[B]" . $namesAPI . "[/B]"  . "\n" .
                "Mikrowisp: " . "[B]" . $idAPI . "[/B]"  . "\n" .
                "Nombre de Red: " . "[B]" . $newNamenet . "[/B]",
            'GROUP_ID' => SERVICE_VENEZUELA_WORKGROUP_ID,
            'CREATED_BY' => TASK_CREATOR_ID,
            'RESPONSIBLE_ID' => $selectedAgent,
            'UF_CRM_TASK' => ""
        ];


        Bitrix24::createTask($fields);

        //That action has not any message, so we advance next action
        static::nextAction($chatId, $actualAction, $actualContextActions);
        $actionType = "voidMsg";

        return [$actionType, $badOptionTxt];
    }

    public static function findAgentAndCreateTask_Fail($chatId, $actualContextActions = '', $actualAction = '', $clientMsg)
    {
        $actionType = "";
        $badOptionTxt = "";

        $selectedAgent = Bitrix24::findAgentB24(SERVICE_VENEZUELA_DEPARTMENT_ID);

        $clientBD = Context::getUserInfo($chatId);
        $fail = $clientBD['FAIL'];
        $failDesc = $clientBD['FAIL_DESC'];
        $namesAPI = $clientBD['NAMES_API'];
        $idAPI = $clientBD['ID_API'];


        $fields = [
            'TITLE' => "Cliente: " . $namesAPI . " - Mikrowisp: " . $idAPI . " - Reporta una Falla",
            'DESCRIPTION' => "Cliente se comunica por una falla de servicio, bajo los siguientes parámetros" . "\n" . "\n" .
                "Título: " . "[B]Falla de Servicios[/B]" . "\n" .
                "Cliente: " . "[B]" . $namesAPI . "[/B]"  . "\n" .
                "Mikrowisp: " . "[B]" . $idAPI . "[/B]"  . "\n" .
                "Falla: " . "[B]" . $fail . "[/B]" . "\n" .
                "Mensaje: " . "[B]" . $failDesc . "[/B]",
            'GROUP_ID' => SERVICE_VENEZUELA_WORKGROUP_ID,
            'CREATED_BY' => TASK_CREATOR_ID,
            'RESPONSIBLE_ID' => $selectedAgent,
            'UF_CRM_TASK' => ""
        ];


        Bitrix24::createTask($fields);

        //That action has not any message, so we advance next action
        static::nextAction($chatId, $actualAction, $actualContextActions);
        $actionType = "voidMsg";

        return [$actionType, $badOptionTxt];
    }

    public static function reqConfirmRequest2($chatId, $actualContextActions = '', $actualAction = '', $clientMsg)
    {
        $actionType = "";
        $badOptionTxt = "";

        switch ($clientMsg) {
            case "1":
                static::nextAction($chatId, $actualAction, $actualContextActions);
                $actionType = "nextIsJustActionThenNormal";
                break;
            case "2":
                static::prevMenu($chatId);
                $actionType = "prevMenu";
                break;

            default:
                $actionType = "badOption";
                $badOptionTxt = "";
                break;
        }

        return [$actionType, $badOptionTxt];
    }

    public static function reqNewPass($chatId, $actualContextActions = '', $actualAction = '', $clientMsg)
    {
        $actionType = "";
        $badOptionTxt = "";

        static::updateUserBD($chatId, "NEW_PASS", $clientMsg);
        static::nextAction($chatId, $actualAction, $actualContextActions);

        $actionType = "requestGood";
        return [$actionType, $badOptionTxt];
    }

    public static function reqNameNet($chatId, $actualContextActions = '', $actualAction = '', $clientMsg)
    {
        $actionType = "";
        $badOptionTxt = "";

        static::updateUserBD($chatId, "NEW_NAMENET", $clientMsg);
        static::nextAction($chatId, $actualAction, $actualContextActions);

        $actionType = "requestGood";
        return [$actionType, $badOptionTxt];
    }

    public static function findAgentAndCreateClaimTask($chatId, $actualContextActions = '', $actualAction = '', $clientMsg)
    {
        $actionType = "";
        $badOptionTxt = "";

        $selectedAgent = Bitrix24::findAgentB24(SERVICE_VENEZUELA_DEPARTMENT_ID);

        $clientBD = Context::getUserInfo($chatId);
        $namesAPI = $clientBD['NAMES_API'];
        $idAPI = $clientBD['ID_API'];
        $claim = $clientBD['CLAIM'];
        $claimDesc = $clientBD['CLAIM_DESC'];

        $fields = [
            'TITLE' => "Cliente: " . $namesAPI . " - Mikrowisp: " . $idAPI . " - Reclamo de Facturación.",
            'DESCRIPTION' => "Cliente se comunica por un reclamo de facturación, bajo los siguientes parámetros" . "\n" . "\n" .
                "Título: " . "[B]Reclamo en Facturación[/B]" . "\n" .
                "Cliente: " . "[B]" . $namesAPI . "[/B]"  . "\n" .
                "Mikrowisp: " . "[B]" . $idAPI . "[/B]"  . "\n" .
                "Título: " . "[B]" . $claim . "[/B]" . "\n" .
                "Reclamo: " . "[B]" . $claimDesc . "[/B]",
            'GROUP_ID' => SERVICE_VENEZUELA_WORKGROUP_ID,
            'CREATED_BY' => TASK_CREATOR_ID,
            'RESPONSIBLE_ID' => $selectedAgent,
            'UF_CRM_TASK' => ""
        ];


        Bitrix24::createTask($fields);

        //That action has not any message, so we advance next action
        static::nextAction($chatId, $actualAction, $actualContextActions);
        $actionType = "voidMsg";

        return [$actionType, $badOptionTxt];
    }


    public static function reqFailDesc($chatId, $actualContextActions = '', $actualAction = '', $clientMsg)
    {
        $actionType = "";
        $badOptionTxt = "";

        static::updateUserBD($chatId, "FAIL_DESC", $clientMsg);
        static::nextAction($chatId, $actualAction, $actualContextActions);

        $actionType = "requestGood";
        return [$actionType, $badOptionTxt];
    }

    protected static function reqFail($chatId, $actualContextActions = '', $actualAction = '', $clientMsg)
    {
        $actionType = "";
        $badOptionTxt = "";

        switch ($clientMsg) {
            case '1': //Pass the context MENU_1.1
                static::updateUserBD($chatId, "FAIL", "Ausencia");
                static::nextAction($chatId, $actualAction, $actualContextActions);
                $actionType = "requestGood";
                break;
            case '2':
                static::updateUserBD($chatId, "FAIL", "Lentitud");
                static::nextAction($chatId, $actualAction, $actualContextActions);
                $actionType = "requestGood";
                break;
            case '3':
                static::updateUserBD($chatId, "FAIL", "Intermitencia");
                static::nextAction($chatId, $actualAction, $actualContextActions);
                $actionType = "requestGood";
                break;
            case '4':
                static::updateUserBD($chatId, "FAIL", "Otro");
                static::nextAction($chatId, $actualAction, $actualContextActions);
                $actionType = "requestGood";
                break;
            case '5':
                static::prevMenu($chatId);
                $actionType = "prevMenu";
                break;
            default:
                $actionType = "badOption";
                $badOptionTxt = "A ingresado un valor incorrecto[BR][BR]";
                break;
        }
        return [$actionType, $badOptionTxt];
    }

    public static function reqConfirmRequest($chatId, $actualContextActions = '', $actualAction = '', $clientMsg)
    {
        $actionType = "";
        $badOptionTxt = "";

        switch ($clientMsg) {
            case "1":
                static::nextAction($chatId, $actualAction, $actualContextActions);
                $actionType = "requestGood";
                break;
            case "2":
                static::prevMenu($chatId);
                $actionType = "prevMenu";
                break;

            default:
                $actionType = "badOption";
                $badOptionTxt = "";
                break;
        }

        return [$actionType, $badOptionTxt];
    }

    protected static function downVelocity($chatId, $actualContextActions = '', $actualAction = '', $clientMsg)
    {
        $actionType = "";
        $badOptionTxt = "";

        $clientBD = Context::getUserInfo($chatId);
        $selectedAgent = Bitrix24::findAgentB24(SERVICE_VENEZUELA_DEPARTMENT_ID);
        $namesAPI = $clientBD['NAMES_API'];
        $idAPI = $clientBD['ID_API'];

        $fields = [
            'TITLE' => "Cliente: " . $namesAPI . " - Mikrowisp: " . $idAPI . " - Solicita Descenso de Velocidad",
            'DESCRIPTION' => "Cliente se comunica solicitando Descenso de velocidad, bajo los siguientes parámetros" . "\n" . "\n" .
                "Título: " . "[B]Descenso de velocidad[/B]" . "\n" .
                "Cliente: " . "[B]" . $namesAPI . "[/B]"  . "\n" .
                "Mikrowisp: " . "[B]" . $idAPI . "[/B]"  . "\n",
            'GROUP_ID' => SERVICE_VENEZUELA_WORKGROUP_ID,
            'CREATED_BY' => TASK_CREATOR_ID,
            'RESPONSIBLE_ID' => $selectedAgent,
            'UF_CRM_TASK' => ""
        ];

        Bitrix24::createTask($fields);

        static::nextAction($chatId, $actualAction, $actualContextActions);
        $actionType = "voidMsg";

        return [$actionType, $badOptionTxt];
    }

    protected static function upVelocity($chatId, $actualContextActions = '', $actualAction = '', $clientMsg)
    {
        $actionType = "";
        $badOptionTxt = "";

        $clientBD = Context::getUserInfo($chatId);

        $selectedAgent = Bitrix24::findAgentB24(SERVICE_VENEZUELA_DEPARTMENT_ID);
        $namesAPI = $clientBD['NAMES_API'];
        $idAPI = $clientBD['ID_API'];

        $fields = [
            'TITLE' => "Cliente: " . $namesAPI . " - Mikrowisp: " . $idAPI . " - Solicita Aumento de Velocidad",
            'DESCRIPTION' => "Cliente se comunica solicitando Aumento de velocidad, bajo los siguientes parámetros" . "\n" . "\n" .
                "Título: " . "[B]Aumento de velocidad[/B]" . "\n" .
                "Cliente: " . "[B]" . $namesAPI . "[/B]"  . "\n" .
                "Mikrowisp: " . "[B]" . $idAPI . "[/B]"  . "\n",
            'GROUP_ID' => SERVICE_VENEZUELA_WORKGROUP_ID,
            'CREATED_BY' => TASK_CREATOR_ID,
            'RESPONSIBLE_ID' => $selectedAgent,
            'UF_CRM_TASK' => ""
        ];

        Bitrix24::createTask($fields);

        static::nextAction($chatId, $actualAction, $actualContextActions);
        $actionType = "voidMsg";

        return [$actionType, $badOptionTxt];
    }

    protected static function reqDesc($chatId, $actualContextActions = '', $actualAction = '', $clientMsg)
    {
        $actionType = "";
        $badOptionTxt = "";

        static::updateUserBD($chatId, "CLAIM_DESC", $clientMsg);
        static::nextAction($chatId, $actualAction, $actualContextActions);

        $actionType = "requestGood";
        return [$actionType, $badOptionTxt];
    }

    protected static function reqTitle($chatId, $actualContextActions = '', $actualAction = '', $clientMsg)
    {
        $actionType = "";
        $badOptionTxt = "";

        static::updateUserBD($chatId, "CLAIM", $clientMsg);
        static::nextAction($chatId, $actualAction, $actualContextActions);

        $actionType = "requestGood";
        return [$actionType, $badOptionTxt];
    }

    protected static function sendMenu11122($chatId, $actualContextActions = '', $actualAction = '', $clientMsg)
    {
        $actionType = "";
        $badOptionTxt = "";

        switch ($clientMsg) {
            case '1':
                static::nextMenu($chatId, "|MENU_1.1.1.2.2.1");
                $actionType = "requestFinal";
                break;
            case '2':
                static::nextMenu($chatId, "|MENU_1.1.1.2.2.2");
                $actionType = "requestFinal";
                break;
            case '3':
                static::nextMenu($chatId, "|MENU_1.1.1.2.2.3");
                $actionType = "requestFinal";
                break;
            case '4':
                static::nextMenu($chatId, "|MENU_1.1.1.2.2.4");
                $actionType = "sendMenu";
                break;
            case '5':
                static::nextMenu($chatId, "|MENU_1.1.1.2.2.5");
                $actionType = "requestFinal";
                break;
            case '6':
                static::prevMenu($chatId);
                $actionType = "prevMenu";
                break;
            default:
                $actionType = "badOption";
                $badOptionTxt = "A ingresado un valor incorrecto[BR][BR]";
                break;
        }
        return [$actionType, $badOptionTxt];
    }

    protected static function sendMenu11121($chatId, $actualContextActions = '', $actualAction = '', $clientMsg)
    {
        $actionType = "";
        $badOptionTxt = "";

        switch ($clientMsg) {
            case '1': //Pass the context MENU_1.1
                static::nextMenu($chatId, "|MENU_1.1.1.2.1.1");
                $actionType = "requestFinal";
                break;
            case '2':
                static::nextMenu($chatId, "|MENU_1.1.1.2.1.2");
                $actionType = "sendMenu";
                break;
            case '3':
                static::nextMenu($chatId, "|MENU_1.1.1.2.1.3");
                $actionType = "nextIsJustActionThenNormal";
                break;
            case '4':
                static::nextMenu($chatId, "|MENU_1.1.1.2.1.4");
                $actionType = "nextIsJustActionThenNormal";
                break;
            case '5':
                static::nextMenu($chatId, "|MENU_1.1.1.2.1.5");
                $actionType = "sendMenu";
                break;
            case '6':
                static::nextMenu($chatId, "|MENU_1.1.1.2.1.6");
                $actionType = "sendMenu";
                break;
            case '7':
                static::nextMenu($chatId, "|MENU_1.1.1.2.1.7");
                $actionType = "requestFinal";
                break;
            case '8':
                static::nextMenu($chatId, "|MENU_1.1.1.2.1.8");
                $actionType = "requestFinal";
                break;
            case '9':
                static::nextMenu($chatId, "|MENU_1.1.1.2.1.9");
                $actionType = "sendMenu";
                break;
            case '10':
                static::prevMenu($chatId);
                $actionType = "prevMenu";
                break;
            default:
                $actionType = "badOption";
                $badOptionTxt = "A ingresado un valor incorrecto[BR][BR]";
                break;
        }
        return [$actionType, $badOptionTxt];
    }

    protected static function findAgentAndCreateTask_Move($chatId, $actualContextActions = '', $actualAction = '', $clientMsg)
    {
        $actionType = "";
        $badOptionTxt = "";

        $selectedAgent = Bitrix24::findAgentB24(SERVICE_VENEZUELA_DEPARTMENT_ID);
        $clientBD = Context::getUserInfo($chatId);
        $whereMove = $clientBD['WHERE_MOVE'];
        $addressMove = $clientBD['ADDRESS_MOVE'];
        $namesAPI = $clientBD['NAMES_API'];
        $idAPI = $clientBD['ID_API'];

        $fields = [
            'TITLE' => "Cliente: " . $namesAPI . " - Mikrowisp: " . $idAPI . " - Solicita Mudanza de Equipos",
            'DESCRIPTION' => "Cliente se comunica solicitando Mudanza de Equipos, bajo los siguientes parámetros" . "\n" . "\n" .
                "Título: " . "[B]Mudanza de Equipos[/B]" . "\n" .
                "Cliente: " . "[B]" . $namesAPI . "[/B]"  . "\n" .
                "Mikrowisp: " . "[B]" . $idAPI . "[/B]"  . "\n" .
                "Tipo: " . "[B]" . $whereMove . "[/B]"  . "\n" .
                "Dirección: " . "[B]" . $addressMove . "[/B]",
            'GROUP_ID' => SERVICE_VENEZUELA_WORKGROUP_ID,
            'CREATED_BY' => TASK_CREATOR_ID,
            'RESPONSIBLE_ID' => $selectedAgent,
            'UF_CRM_TASK' => ""
        ];


        Bitrix24::createTask($fields);

        //That action has not any message, so we advance next action
        static::nextAction($chatId, $actualAction, $actualContextActions);
        $actionType = "voidMsg";

        return [$actionType, $badOptionTxt];
    }

    protected static function reqConfirm($chatId, $actualContextActions = '', $actualAction = '', $clientMsg)
    {
        $actionType = "";
        $badOptionTxt = "";
        switch ($clientMsg) {
            case '1': //Pass the context MENU_1.1
                static::nextAction($chatId, $actualAction, $actualContextActions);
                $actionType = "nextIsJustActionThenNormal";
                break;
            case '2':
                static::prevMenu($chatId);
                $actionType = "prevMenu";
                break;
            default:
                $actionType = "badOption";
                $badOptionTxt = "A ingresado un valor incorrecto[BR][BR]";
                break;
        }
        return [$actionType, $badOptionTxt];
    }

    protected static function reqAddress($chatId, $actualContextActions = '', $actualAction = '', $clientMsg)
    {
        $actionType = "";
        $badOptionTxt = "";

        static::updateUserBD($chatId, "ADDRESS_MOVE", $clientMsg);
        static::nextAction($chatId, $actualAction, $actualContextActions);

        $actionType = "requestGood";
        return [$actionType, $badOptionTxt];
    }

    protected static function reqWhere($chatId, $actualContextActions = '', $actualAction = '', $clientMsg)
    {
        $actionType = "";
        $badOptionTxt = "";
        switch ($clientMsg) {
            case '1': //Pass the context MENU_1.1
                static::updateUserBD($chatId, "WHERE_MOVE", "Casa");
                static::nextAction($chatId, $actualAction, $actualContextActions);
                $actionType = "requestGood";
                break;
            case '2':
                static::updateUserBD($chatId, "WHERE_MOVE", "Apartamento");
                static::nextAction($chatId, $actualAction, $actualContextActions);
                $actionType = "requestGood";
                break;
            case '3':
                static::updateUserBD($chatId, "WHERE_MOVE", "Comercio");
                static::nextAction($chatId, $actualAction, $actualContextActions);
                $actionType = "requestGood";
                break;
            case '4':
                static::updateUserBD($chatId, "WHERE_MOVE", "Otro");
                static::nextAction($chatId, $actualAction, $actualContextActions);
                $actionType = "requestGood";
                break;
            case '5':
                static::prevMenu($chatId);
                $actionType = "prevMenu";
                break;
            default:
                $actionType = "badOption";
                $badOptionTxt = "A ingresado un valor incorrecto[BR][BR]";
                break;
        }
        return [$actionType, $badOptionTxt];
    }

    protected static function sendMenu111212($chatId, $actualContextActions = '', $actualAction = '', $clientMsg)
    {
        $actionType = "";
        $badOptionTxt = "";
        switch ($clientMsg) {
            case '1': //Pass the context MENU_1.1
                static::nextMenu($chatId, "|MENU_1.1.1.2.1.2.1");
                $actionType = "sendMenu";
                break;
            case '2':
                static::prevMenu($chatId);
                $actionType = "prevMenu";
                break;
            default:
                $actionType = "badOption";
                $badOptionTxt = "A ingresado un valor incorrecto[BR][BR]";
                break;
        }
        return [$actionType, $badOptionTxt];
    }

    protected static function sendMenu111($chatId, $actualContextActions = '', $actualAction = '', $clientMsg)
    {
        $actionType = "";
        $badOptionTxt = "";
        switch ($clientMsg) {
            case '1': //Pass the context MENU_1.1
                static::nextMenu($chatId, "|MENU_1.1.1.1");
                $actionType = "requestFinal";
                break;
            case '2':
                static::nextMenu($chatId, "|MENU_1.1.1.2");
                $actionType = "sendMenu";
                break;
            case '3':
                static::nextMenu($chatId, "|MENU_1.1.1.3");
                $actionType = "requestFinal";
                break;
            case '4':
                static::prevMenu($chatId);
                $actionType = "prevMenu";
                break;
            default:
                $actionType = "badOption";
                $badOptionTxt = "A ingresado un valor incorrecto[BR][BR]";
                break;
        }
        return [$actionType, $badOptionTxt];
    }

    protected static function sendMenu11($chatId, $actualContextActions = '', $actualAction = '', $clientMsg)
    {
        $actionType = "";
        $badOptionTxt = "";
        $bytes = file_put_contents("context.json", $context);

        switch ($clientMsg) {
            case '1':
                static::nextMenu($chatId, "|MENU_1.1.1");
                $actionType = "sendMenu";
                break;
            case '2':
                static::nextMenu($chatId, "|MENU_1.1.2");
                $actionType = "sendMenu";
                break;
            default:
                $actionType = "badOption";
                $badOptionTxt = "A ingresado un valor incorrecto[BR][BR]";
                break;
        }
        return [$actionType, $badOptionTxt];
    }

    protected static function reqPhoneRef($chatId, $actualContextActions = '', $actualAction = '', $clientMsg)
    {
        $actionType = "";
        $badOption = "";

        static::updateUserBD($chatId, "PHONE_REF", "$clientMsg");
        static::nextAction($chatId, $actualAction, $actualContextActions);

        $actionType = "requestGood";
        return [$actionType, $badOption];
    }

    protected static function findAgentAndCreateReferredLead($chatId, $actualContextActions, $actualAction, $clientMsg)
    {
        $actionType = "";
        $badOptionTxt = "";
        $clientBD = Context::getUserInfo($chatId);
        $telephoneRef = $clientBD['PHONE_REF']; //Obtaining phone referred
        $namesRef = $clientBD['NAMES_REF']; //Obtaining names referred

        //Selecting agent from department
        $selectedAgent = Bitrix24::findAgentB24(SOCIAL_VENEZUELA_DEPARTMENT_ID);

        $arLead = [
            'TEL'           => $telephoneRef,
            'NAMES'         => $namesRef,
            'RESPONSIBLE'   => $selectedAgent
        ];

        //Creating Lead
        $leadIdCreated = Bitrix24::createLead($arLead);

        $fields = [
            'TITLE' => "Atención a Referido - CS VE - " . $telephoneRef . " - " . $namesRef,
            'DESCRIPTION' => "Cliente brinda referido bajo las siguientes especificaciones." . "\n" . "\n" .
                "[B]Cliente:[/B] " . $namesRef . "\n" .
                "[B]Teléfono:[/B] " . $telephoneRef,
            'GROUP_ID' => SOCIAL_VENEZUELA_WORKGROUP_ID,
            'CREATED_BY' => TASK_CREATOR_ID,
            'RESPONSIBLE_ID' => $selectedAgent,
            'UF_CRM_TASK' => "L_" . $leadIdCreated
        ];

        Bitrix24::createTask($fields);

        //That action has not any message, so we advance next action
        static::nextAction($chatId, $actualAction, $actualContextActions);
        $actionType = "voidMsg";

        return [$actionType, $badOptionTxt];
    }

    protected static function reqNamesRef($chatId, $actualContextActions, $actualAction, $clientMsg)
    {
        $actionType = "";
        $badOptionTxt = "";

        static::updateUserBD($chatId, "NAMES_REF", $clientMsg);
        static::nextAction($chatId, $actualAction, $actualContextActions);

        $actionType = "firstJustActionThenBye";
        return [$actionType, $badOptionTxt];
    }

    protected static function reqRif($chatId, $actualContextActions, $actualAction, $clientMsg)
    {
        $actionType = "";
        $badOptionTxt = "";

        $result = Mikrowisp::rif_exists($clientMsg);

        if (count($result) === 1) {
            $rifTries = Context::getUserInfo($chatId)['RIF_TRIES'];
            if ($rifTries == 0) {
                static::updateUserBD($chatId, "RIF_TRIES", 1);
                $badOptionTxt = "notFoundRif";
                $actionType = "noChangeAction";
            } elseif ($rifTries == 1) {
                static::updateUserBD($chatId, "RIF_TRIES", 2);
                $badOptionTxt = "notFoundRif";
                $actionType = "noChangeAction";
            } elseif ($rifTries == 2) {
                static::updateUserBD($chatId, "RIF_TRIES", 3);
                $badOptionTxt = "notFoundRif2";
                $actionType = "noChangeActionPrevMenu";
            } else {
                static::updateUserBD($chatId, "RIF_TRIES", 0);
                static::prevMenu($chatId);
                $actionType = "prevMenu";
            }
        } else {
            static::updateUserBD($chatId, "ACTION", "sendMenu111");
            static::updateUserBD($chatId, "RIF", $clientMsg);
            static::updateUserBD($chatId, "ID_API", $result['id']);
            static::updateUserBD($chatId, "NAMES_API", $result['nombre']);
            static::updateUserBD($chatId, "STATE_API", $result['estado']);
            static::updateUserBD($chatId, "PHONE_API", $result['telefono']);
            static::updateUserBD($chatId, "PASSWORD_API", $result['password']);
            static::updateUserBD($chatId, "SERVICE_API", $result['servicio']);
            static::updateUserBD($chatId, "FACNP_API", $result['fac_np']);
            static::updateUserBD($chatId, "FACTOTAL_API", $result['fac_total']);
            $actionType = "sendMenuReplacingData";
        }


        return [$actionType, $badOptionTxt];
    }

    protected static function reqNames($chatId, $actualContextActions, $actualAction, $clientMsg)
    {
        $actionType = "";
        $badOptionTxt = "";

        static::updateUserBD($chatId, "NAMES", $clientMsg);
        static::nextAction($chatId, $actualAction, $actualContextActions);

        //Creating Lead
        // $clientBD       =   Context::getUserInfo($chatId);

        // file_put_contents('JsonReqNames-'.$actualAction.'.json', json_encode([
        //     'chatId'        =>  $chatId,
        //     'clientMsg'     =>  $clientMsg,
        //     'actualAction'  =>  $actualAction,
        //     'actualContextActions'  =>  $actualContextActions,
        //     'clientBD'      =>  $clientBD
        // ]));


        // $telephone      =   $clientBD['PHONE']; //Obtaining phone
        // $dni            =   $clientBD['DNI'];
        // $names          =   $clientBD['NAMES']; //Obtaining names
        // $selectedAgent  =   Bitrix24::findAgentB24(SALES_VENEZUELA_DEPARTMENT_ID);

        // $arLead = [
        //     'TEL'           =>  $telephone,
        //     'NAMES'         =>  $names,
        //     'DNI'           =>  $dni,
        //     'RESPONSIBLE'   =>  $selectedAgent
        // ];

        // //Creating Lead
        // $leadIdCreated = Bitrix24::createLead($arLead);

        $actionType = "requestGood";
        // $actionType = "requestFinal";
        return [$actionType, $badOptionTxt];
    }

    protected static function reqPhone($chatId, $actualContextActions, $actualAction, $clientMsg)
    {
        $actionType = "";
        $badOption = "";

        static::updateUserBD($chatId, "PHONE", "$clientMsg");
        static::nextAction($chatId, $actualAction, $actualContextActions);

        $actionType = "requestGood";
        return [$actionType, $badOption];
    }

    protected static function reqDni($chatId, $actualContextActions, $actualAction, $clientMsg)
    {
        $actionType = "";
        $badOption  = "";

        static::updateUserBD($chatId, "DNI", $clientMsg);
        static::nextAction($chatId, $actualAction, $actualContextActions);

        $clientBD       =   Context::getUserInfo($chatId);

        $telephone      =   $clientBD['PHONE']; //Obtaining phone
        $dni            =   $clientBD['DNI'];
        $names          =   $clientBD['NAMES']; //Obtaining names

        $selectedAgent  =   Bitrix24::findAgentB24(SALES_VENEZUELA_DEPARTMENT_ID);

        $arLead = [
            'TEL'           =>  $telephone,
            'NAMES'         =>  $names,
            'DNI'           =>  $dni,
            'RESPONSIBLE'   =>  $selectedAgent
        ];

        //Creating Lead
        // $leadIdCreated = Bitrix24::createLeadFirst($arLead);

        // $actionType = "requestGood";
        $actionType = "requestFinal";
        return [$actionType, $badOption];
    }

    protected static function sendMenu1($chatId, $actualContextActions = '', $actualAction = '', $clientMsg)
    {
        $actionType = "";
        $badOptionTxt = "";
        switch ($clientMsg) {
            case '1': //Pass the context MENU_1.1
                static::nextMenu($chatId, "|MENU_1.1");
                $actionType = "sendMenu";
                break;

            case '2':
                static::nextMenu($chatId, "|MENU_1.2");
                $actionType = "sendMenu";
                break;

            default:
                $actionType = "badOption";
                $badOptionTxt = "A ingresado un valor incorrecto, ";
                break;
                
        }
        return [$actionType, $badOptionTxt];
    }

    public static function byeBot($chatId = '')
    {
        $actionType = "justAction";
        static::updateUserBD($chatId, "CONTEXT", "MENU_1");
        static::updateUserBD($chatId, "ACTION", "");

        $deleteChat = True;

        $badOptionTxt = "";
        return [$actionType, $badOptionTxt];
    }
}
