<?php

const MENU_RULES = "rules.json";

class Context
{
	public $menu;

	public function __construct($menu)
	{
		$this->menu = $menu;
	}

	public function setMenu($menu)
	{
		$this->menu = $menu;
	}

	public static function testClass($msg)
	{
		return "Soy context::testclass y me dijiste: " . $msg;
	}


	public static function replace_if_exists($chatId, $msg = '')
	{
		$return = "";
		$answer = $msg;
		if (str_contains($msg, '{{') && str_contains($msg, '}}')) {

			$userData = static::getUserInfo($chatId);

			$arDataToReplace = [
				"RIF" 			=> 	"{{rif}}",
				"ID_API" 		=> 	"{{id}}",
				"NAMES_API" 	=> 	"{{nombre}}",
				"STATE_API" 	=> 	"{{estado}}",
				"PHONE_API" 	=> 	"{{telefono}}",
				"PASSWORD_API" 	=> 	"{{password}}",
				"SERVICE_API" 	=> 	"{{servicio}}",
				"FACNP_API" 	=> 	"{{fac_np}}",
				"FACTOTAL_API" 	=> 	"{{fac_total}}"
			];

			foreach ($arDataToReplace as $key => $value) {
				$answer = str_replace($value, $userData[$key], $answer);
			}
			$return = $answer;
		} else {
			$return = $msg;
		}

		return $return;
	}

	public static function getUserInfo($chatId)
	{
		// $arUserBD 	=	[];
		$file = $chatId . ".json"; //information of ChatID in a Json file
		$path = dirname(__FILE__, 3) . "/" . "json/";

		//Obtaining the actual action from the ChatID
		$arUserBD = static::getFile($path, $file); //The chatid information array

		return $arUserBD;

	}


	public static function contextNow($chatId)
	{
		$context = ""; //What action will be executed from the User BD Json.
		$file = $chatId . ".json"; //information of ChatID in a Json file
		$path = dirname(__FILE__, 3) . "/" . "json/";

		//Obtaining the actual action from the ChatID
		$arUserBD = static::getFile($path, $file); //The chatid information array
		$context = $arUserBD["CONTEXT"];

		$bytes = file_put_contents("context.json", $context);

		return $context;
	}


	public static function actionNow($chatId, $actualContextActions)
	{
		$action = ""; //What action will be executed from the User BD Json.
		$file = $chatId . ".json"; //information of ChatID in a Json file
		$path = dirname(__FILE__, 3) . "/" . "json/";

		//Obtaining the actual action from the ChatID
		$arUserBD = static::getFile($path, $file); //The chatid information array
		$action = $arUserBD["ACTION"];

		if ($action == "") {
			$action = array_key_first($actualContextActions);
			$arUserBD["ACTION"] = $action;
			static::setFile($path, $file, $arUserBD);
		} else {
			$action = $arUserBD["ACTION"];
		}

		return $action;
	}

	public static function findContextActions($chatId = '')
	{
		$arUserBD = []; // Array to capture the BD fron BD.json
		$actualContext = ""; //Context to return example "INITIAL|MENU|MENU1|ETC"
		$arActions = []; // Array to get the actions based on $actualContex
		$file = $chatId . ".json"; //information of ChatID in a Json file
		$path = dirname(__FILE__, 3) . "/" . "json/";
		if (file_exists($path . $file)) {
			$arUserBD = static::getFile($path, $file); //The chatid information array
			$actualContext = $arUserBD["CONTEXT"];

			//test
			//return ["En archivo:", $path . $file, "obtengo:", json_encode($arUserBD), "y el contexto:", $actualContext];
		} else {
			$arUserBD = [
				"CONTEXT" => "MENU_1",
				"ACTION" => "",
				"NAMES" => "",
				"PHONE" => "",
				"DNI" => "",
				"LAST_CONVERSATION" => "",
				"ATTEMPTS" => 0,
				"RIF" => "",
				"PHONE_REF" => "",
				"NAMES_REF" => "",
				"RIF_TRIES" => 0,
				"WHATSAPP_TRIES" => 0,
				"WHERE_MOVE" => "",
				"ADDRESS_MOVE" => "",
				"ID_API" => "",
				"NAMES_API" => "",
				"STATE_API" => "",
				"PHONE_API" => "",
				"PASSWORD_API" => "",
				"SERVICE_API" => "",
				"FACNP_API" => "",
				"FACTOTAL_API" => "",
				"NEW_PASS" => "",
				"NEW_NAMENET" => "",
				"FAIL" => "",
				"FAIL_DESC" => "",
				"CLAIM" => "",
				"CLAIM_DESC" => ""

			];
			static::setFile($path, $file, $arUserBD);
			$actualContext = static::getFile($path, $file)["CONTEXT"];

			//test
			//return ["No encontré el archivo:", $path . $file, "Creo el array", json_encode($arUserBD), "y el contexto:", $actualContext];
		}

		$arActions = static::getContextActions($actualContext);
		return $arActions;
	}

	public static function getContextActions($actualContext = '')
	{
		$arActions = [];
		$file = MENU_RULES;
		$path = dirname(__FILE__, 3) . "/";
		$arRules = static::getFile($path, $file); //rules obtained from json
		$arRoute = static::getRoute($actualContext);

		$arActions = $arRules;
		foreach ($arRoute as $key => $value) {
			$arActions = $arActions[$value];
		}
		$arActions = $arActions["ACTIONS"]; //Obtaining the actions of the actual context
		return $arActions;
	}

	protected static function getRoute($actualContext = '')
	{
		$arRoute = [];
		$arRoute = explode("|", $actualContext);
		return $arRoute;
	}


	public  static function getFile($path, $file)
	{
		$arFile = [];
		if (file_exists($path . $file)) 
		{
			$content = file_get_contents($path . $file);
			$arFile = json_decode($content, true);
		}

			file_put_contents('JsonGetFile-'.$path.'-'.$file.'.json', json_encode([
	            'path'      =>  $path,
	            'file'     	=>  $file,
	            'route'		=>  $path . $file,
	            'content'  	=>  $content,
	            'arFile'    =>  $arFile
	        ]));

		return $arFile;
	}

	public static function setFile($path, $file, $arFile)
	{
		//$path = dirname(__FILE__, 3) . "/json/$file";
		if (file_exists($path . $file)) {
			$content = json_encode($arFile);
			file_put_contents($path . $file, $content);
		} else {
			file_put_contents($path . $file, json_encode($arFile));
			//give write permissions to file
			//...
		}
	}
}
