<?php

error_reporting(E_ALL);
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);

require_once(__DIR__ . '/crest.php');
require_once(__DIR__ . '/app/Controllers/Context.php');
require_once(__DIR__ . '/app/Controllers/Bot.php');
require_once(__DIR__ . '/app/Controllers/Mikrowisp.php');
require_once(__DIR__ . '/app/Controllers/Bitrix24.php');
require_once(__DIR__ . '/app/Controllers/DBBot.php');

switch (strtoupper($_REQUEST['event'])) {

	case 'ONIMBOTJOINCHAT':

		break;
	case 'ONIMBOTMESSAGEADD':

		//Crear un archivo json e ir almacenando un log con el tiempo por cada vez que responda el usuario. Si la diferencia entre el último log y reciente es 5min o más, reiniciar nuevamente.

		//Capture the option typed by person
		$clientMsg = $_REQUEST['data']['PARAMS']['MESSAGE'];

		//Capture the chat ID
		$chatId = $_REQUEST['data']['PARAMS']['DIALOG_ID'];

		//Deleting the User BD(info) if it is closed.
		deleteClosed($chatId);
		

		//Actual context from userList json for the Actual CHAT ID
		$actualContextActions = Context::findContextActions($chatId);
		//$actualContextActions = Context::findContextActions($clientMsg);

		//setting the first actions for new users and obtain the action from registered
		$actualAction = Context::actionNow($chatId, $actualContextActions);
		//$actualAction = Context::actionNow($clientMsg, $actualContextActions);

		//$arAnswer = Bot::executeAction($clientMsg, $actualContextActions, $actualAction, $clientMsg);
		$arAnswer = Bot::executeAction($chatId, $actualContextActions, $actualAction, $clientMsg);

		//$test = ["Chat ID: " . $clientMsg, "Context actual: " . json_encode($actualContextActions), "Acción actual: " . $actualAction];
		answer($chatId, $arAnswer, $actualContextActions, $actualAction);

		//FOR DIRECTLY TEST
		/**
		$arReq = Bot::testClass('sdf');

		foreach ($arReq as $key => $value) {
			CRest::call(
				'imbot.message.add',
				[
					'DIALOG_ID' => $_REQUEST['data']['PARAMS']['DIALOG_ID'],
					'MESSAGE' => $value,
				]
			);
		}


	
		//TESTING API

		$clientMsg = $_REQUEST['data']['PARAMS']['MESSAGE'];

		$resultAPI = Mikrowisp::rif_exists($clientMsg);

		CRest::call(
			'imbot.message.add',
			[
				'DIALOG_ID' => $_REQUEST['data']['PARAMS']['DIALOG_ID'],
				'MESSAGE' => $resultAPI,
			]
		);

		 
		//TESTING DELETED CHATS

		$chatId = $_REQUEST['data']['PARAMS']['DIALOG_ID'];

		unlink("json/" . $chatId . ".json");
		 */
		break;
}

function deleteClosed($chatId)
{
	if (Context::getUserInfo($chatId)['ACTION'] == "byeBot") {
		unlink("json/" . $chatId . ".json");
	}
	return true;
}

function answer($chatId, $arAnswer, $actualContextActions, $actualAction)
{
	$justMssg = $arAnswer[0];
	$arMessages = $arAnswer[1];
	$arResultAction = $arAnswer[2];

	if ($justMssg) {
		foreach ($arMessages as $key => $value) {
			CRest::call(
				'imbot.message.add',
				[
					'DIALOG_ID' => $_REQUEST['data']['PARAMS']['DIALOG_ID'],
					'MESSAGE' => $value,
				]
			);
		}
	} else {
		$actionType = $arResultAction[0];
		$badOptionTxt = $arResultAction[1];

		switch ($actionType) {
			case "badOption":
				CRest::call(
					'imbot.message.add',
					[
						'DIALOG_ID' => $_REQUEST['data']['PARAMS']['DIALOG_ID'],
						'MESSAGE' => $badOptionTxt . lcfirst($actualContextActions[$actualAction]),
					]
				);
				break;
			case "sendMenu":
				$newContextActions = Context::findContextActions($chatId);

				CRest::call(
					'imbot.message.add',
					[
						'DIALOG_ID' => $_REQUEST['data']['PARAMS']['DIALOG_ID'],
						'MESSAGE' => $newContextActions[array_key_first($newContextActions)],
					]
				);

				break;

			case "sendMenuReplacingData":

				$newContextActions = Context::findContextActions($chatId);
				$newActualAction = Context::actionNow($chatId, $newContextActions);

				$answer = $newContextActions[$newActualAction];

				$answer = Context::replace_if_exists($chatId, $answer);
				CRest::call(
					'imbot.message.add',
					[
						'DIALOG_ID' => $_REQUEST['data']['PARAMS']['DIALOG_ID'],
						'MESSAGE' => $answer,
					]
				);

				break;


			case "noChangeAction":
				$newContextActions = Context::findContextActions($chatId);
				CRest::call(
					'imbot.message.add',
					[
						'DIALOG_ID' => $_REQUEST['data']['PARAMS']['DIALOG_ID'],
						'MESSAGE' => $newContextActions[$badOptionTxt],
					]
				);

				$newActualAction = Context::actionNow($chatId, $newContextActions);

				CRest::call(
					'imbot.message.add',
					[
						'DIALOG_ID' => $_REQUEST['data']['PARAMS']['DIALOG_ID'],
						'MESSAGE' => $newContextActions[$newActualAction],
					]
				);

				break;

			case "validateWhatsApp":

				$newContextActions = Context::findContextActions($chatId);
				$newActualAction = Context::actionNow($chatId, $newContextActions);

				$finalAnswer = Bot::executeAction($chatId, $newContextActions, $newActualAction, "");

				$miniActionType = $finalAnswer[2][0];
				$selectedAgentId = $finalAnswer[2][1];

				switch ($miniActionType) {
					case "notFoundAvailable":
						CRest::call(
							'imbot.message.add',
							[
								'DIALOG_ID' => $_REQUEST['data']['PARAMS']['DIALOG_ID'],
								'MESSAGE' => $newContextActions[$miniActionType],
							]
						);

						Bot::prevMenu($chatId);
						$newContextActions = Context::findContextActions($chatId);

						CRest::call(
							'imbot.message.add',
							[
								'DIALOG_ID' => $_REQUEST['data']['PARAMS']['DIALOG_ID'],
								'MESSAGE' => $newContextActions[array_key_last($newContextActions)],
							]
						);

						break;
					case "notFoundAvailable2":
						$newContextActions = Context::findContextActions($chatId);

						CRest::call(
							'imbot.message.add',
							[
								'DIALOG_ID' => $_REQUEST['data']['PARAMS']['DIALOG_ID'],
								'MESSAGE' => $newContextActions[$miniActionType],
							]
						);

						Bot::prevMenu($chatId);
						Bot::prevMenu($chatId);
						$newContextActions = Context::findContextActions($chatId);

						CRest::call(
							'imbot.message.add',
							[
								'DIALOG_ID' => $_REQUEST['data']['PARAMS']['DIALOG_ID'],
								'MESSAGE' => $newContextActions[array_key_last($newContextActions)],
							]
						);

						break;
					case "transferToOperator":

						CRest::call(
							'imbot.message.add',
							[
								'DIALOG_ID' => $_REQUEST['data']['PARAMS']['DIALOG_ID'],
								'MESSAGE' => "Su conversación está siendo transferida hacia uno de nuestros agentes en linea, en breves instantes se comunicará con Usted. Bibi se despide de esta conversación 🤖"
							]
						);


						CRest::call('imopenlines.bot.session.transfer', [
							'CHAT_ID' 	=> 	$_REQUEST['data']['PARAMS']['CHAT_ID'],
							'USER_ID' 	=> 	$selectedAgentId,
							'LEAVE' 	=> 	'Y'

						]);
						break;
				}
				break;
			case "noChangeActionPrevMenu":
				$newContextActions = Context::findContextActions($chatId);
				CRest::call(
					'imbot.message.add',
					[
						'DIALOG_ID' => $_REQUEST['data']['PARAMS']['DIALOG_ID'],
						'MESSAGE' => $newContextActions[$badOptionTxt],
					]
				);

				$newActualAction = Context::actionNow($chatId, $newContextActions);
				Bot::executeAction($chatId, $newContextActions, $newActualAction, "");
				$newContextActions = Context::findContextActions($chatId);

				CRest::call(
					'imbot.message.add',
					[
						'DIALOG_ID' => $_REQUEST['data']['PARAMS']['DIALOG_ID'],
						'MESSAGE' => $newContextActions[array_key_last($newContextActions)],
					]
				);

				break;

			case "prevMenu":

				$newContextActions = Context::findContextActions($chatId);

				$answer = $newContextActions[array_key_last($newContextActions)];
				$answer = Context::replace_if_exists($chatId, $answer);

				CRest::call(
					'imbot.message.add',
					[
						'DIALOG_ID' => $_REQUEST['data']['PARAMS']['DIALOG_ID'],
						'MESSAGE' => $answer,
					]
				);

				break;
			case "justAction":



				break;
			case "requestGood":

				$newContextActions = Context::findContextActions($chatId);
				$newActualAction = Context::actionNow($chatId, $newContextActions);

				CRest::call(
					'imbot.message.add',
					[
						'DIALOG_ID' => $_REQUEST['data']['PARAMS']['DIALOG_ID'],
						'MESSAGE' => $newContextActions[$newActualAction],
					]
				);

				break;
			case "firstJustActionThenBye":
				$newContextActions = Context::findContextActions($chatId);
				$newActualAction = Context::actionNow($chatId, $newContextActions);
				$finalAnswerMenu = Bot::executeAction($chatId, $newContextActions, $newActualAction, "");

				if ($finalAnswerMenu[2][0] = "voidMsg") {
					$newContextActions = Context::findContextActions($chatId);
					$newActualAction = Context::actionNow($chatId, $newContextActions);
					Bot::executeAction($chatId, $newContextActions, $newActualAction, "");

					CRest::call(
						'imbot.message.add',
						[
							'DIALOG_ID' => $_REQUEST['data']['PARAMS']['DIALOG_ID'],
							'MESSAGE' => $newContextActions[$newActualAction],
						]
					);
				}

				break;
			case "requestFinal":
				$newContextActions = Context::findContextActions($chatId);
				$newActualAction = Context::actionNow($chatId, $newContextActions);
				$finalAnswerMenu = Bot::executeAction($chatId, $newContextActions, $newActualAction, "");

				$arNewMssg = $finalAnswerMenu[1];

				foreach ($arNewMssg as $key => $value) {
					$answer = Context::replace_if_exists($chatId, $value);
					CRest::call(
						'imbot.message.add',
						[
							'DIALOG_ID' => $_REQUEST['data']['PARAMS']['DIALOG_ID'],
							'MESSAGE' => $answer,
						]
					);
				}

				break;


			case "nextIsJustActionThenNormal":

				$newContextActions = Context::findContextActions($chatId);
				$newActualAction = Context::actionNow($chatId, $newContextActions);

				$finalAnswer = Bot::executeAction($chatId, $newContextActions, $newActualAction, "");

				if ($finalAnswer[2][0] = "voidMsg") {
					$newContextActions = Context::findContextActions($chatId);
					$newActualAction = Context::actionNow($chatId, $newContextActions);
					$finalAnswer = Bot::executeAction($chatId, $newContextActions, $newActualAction, "");

					$arNewMssg = $finalAnswer[1];

					foreach ($arNewMssg as $key => $value) {
						CRest::call(
							'imbot.message.add',
							[
								'DIALOG_ID' => $_REQUEST['data']['PARAMS']['DIALOG_ID'],
								'MESSAGE' => $value,
							]
						);
					}
				}



				break;


			case "development":
				CRest::call(
					'imbot.message.add',
					[
						'DIALOG_ID' => $_REQUEST['data']['PARAMS']['DIALOG_ID'],
						'MESSAGE' => $badOptionTxt,
					]
				);
		}
	}
}